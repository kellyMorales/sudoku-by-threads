// Author: Kelly Morales
// CS 450 - Operating Systems
// Project 2: Sudoku
// This program accepts an input file with a completed sudoku grid, then
// determines if the grid is a valid sudoku or not. This program uses multiple
// threads.

#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void readInput(int grid[9][9]);
bool checkInput(int grid[9][9]);
void checkRow(int grid[9][9], int row, int *isValid);
void checkCol(int grid[9][9], int col, int *isValid);
void check3x3(int grid[9][9], int row, int col, int *isValid);

int main(){
	int grid[9][9];
	readInput(grid);
	bool isValid = checkInput(grid);
	if (isValid) cout << "The input is a Sudoku." << endl;
	else cout << "The input is NOT Sudoku." << endl;
  return 0;
}

void readInput(int grid[9][9]){
	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 9; j++){
			cin >> grid[i][j];
		}
	}	
}

bool checkInput(int grid[9][9]){
	int isValid = 1;
	vector<thread> all_threads;
	for (int i = 0; i < 9; i++){ // for every row
		all_threads.push_back(thread(checkRow, grid, i, &isValid));
	}
	for (int i = 0; i < 9; i++){ // for every column
		all_threads.push_back(thread(checkCol, grid, i, &isValid));
	}
	for (int i = 0; i < 9; i+=3){ // every upper left row
		for (int j = 0; j < 9; j+=3){ // every upper left column
			all_threads.push_back(thread(check3x3, grid, i, j, &isValid));
		}
	}
	for (int i = 0; i < all_threads.size(); i++){
		all_threads[i].join();
	}
	if (isValid) return true;
	else return false;
}

void checkRow(int grid[9][9], int row, int *isValid){
	for (int i = 1; i <= 9; i++){ // compare to nums 1-9
		int numCorrect = 0;
		for (int j = 0; j < 9; j++){ // every col in row
			if (grid[row][j] == i){
				numCorrect++;
			}
		}
		if (numCorrect != 1){
			*isValid = 0;
			cout << "Row " << row+1 << " doesn't have the required values." << endl;
			return;
		}
		else{
			*isValid = 1;
		}
	}
}

void checkCol(int grid[9][9], int col, int *isValid){
	for (int i = 1; i <= 9; i++){
		int numCorrect = 0;
		for (int j = 0; j < 9; j++){
			if (grid[j][col] == i)
				numCorrect++;
		}
		if (numCorrect != 1){
			*isValid = 0;
			cout << "Column " << col+1 << " doesn't have the required values." << endl;
			return;
		}
		else *isValid = 1;
	}
}

void check3x3(int grid[9][9], int row, int col, int *isValid){
	for (int k = 1; k <= 9; k++){
		int numCorrect = 0;
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				if (grid[row + i][col + j] == k)
					numCorrect++;
			}
		}
		if (numCorrect != 1){
			*isValid = 0;
			cout << "Grid (" << row+1 << ", " << col+1 << ") doesn't have the required values." << endl;
			return;
		}
		else *isValid = 1;
	}
}